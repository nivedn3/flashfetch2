import requests
import sys
from bs4 import BeautifulSoup
link= sys.argv[1]
request=requests.get(link)
soup=BeautifulSoup(request.content,"lxml")
if "amazon" in str(link):
	product_name=soup.find_all("span",{"id":"productTitle"})
	product_price=soup.find_all("span",{"class":"a-size-medium a-color-price"})
	product_cat=soup.find_all("a",{'class':'a-link-normal a-color-tertiary'})
	print "product name "  + product_name
	print "product price " + product_price
	print "product cat " + product_cat
	sys.exit()


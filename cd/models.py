from __future__ import unicode_literals

from django.db import models

# Create your models here.
class request_conf(models.Model):
	Cus_link=models.URLField()
	Ser_product=models.CharField(max_length=50)
	Ser_image=models.URLField()
	cus_category=models.CharField(max_length=50,default=1)
	Cus_email=models.CharField(max_length=50,null=True)
	Ser_price=models.CharField(max_length=50)
	Cus_expiry=models.IntegerField(null=True)
	Cus_id=models.CharField(max_length=32,primary_key=True)
	cus_loc=models.CharField(max_length=100,null=True)
	Cus_name=models.CharField(max_length=100,null=True)
	time=models.BigIntegerField(default=100)

class sellerlogindb(models.Model):
	user=models.CharField(max_length=50)
	password=models.CharField(max_length=50)
	email=models.EmailField(primary_key=True)
	mobile=models.BigIntegerField()
	shopname=models.CharField(max_length=80)
	shopid=models.CharField(max_length=80)
	office_no=models.IntegerField(null=True)
	city=models.CharField(max_length=200,null=True)
	Address1=models.CharField(max_length=200,null=True)
	Address2=models.CharField(max_length=200,null=True)
	sel_loc=models.CharField(max_length=100,null=True)
	category=models.BigIntegerField(default=1)
	gcmid=models.CharField(max_length=1000,null=True)
	token=models.CharField(max_length=100,null=True)
	gps=models.CharField(max_length=100,null=True)
        imagepath=models.CharField(max_length=200,null=True)
	decline=models.CharField(max_length=60000,default="123")
	returns=models.BigIntegerField(default=1)
	wallet_sel=models.IntegerField(default=0)
	uniqid_sel=models.CharField(max_length=8,default=0)
	reco_code_sel=models.CharField(max_length=6,null=True)
	fpass_sel=models.IntegerField(default=0)
        returnpolicy=models.IntegerField(default=1)
	access=models.CharField(max_length=20,default="FFPRIME")

class customerlogindb(models.Model):
	user=models.CharField(max_length=50)
	password=models.CharField(max_length=50)
	email=models.EmailField(primary_key=True)
	mobile=models.BigIntegerField(null=True)
	gcmid=models.CharField(max_length=1000)
	token=models.CharField(max_length=100)
	mailver=models.IntegerField(default=0)
	###NEW FIELDS ########
	fpass=models.IntegerField(default=0)
	wallet=models.IntegerField(default=0)
	#TO BE MADE PRIMARY
	uniqid=models.CharField(max_length=6,null=True)
	transactions_no=models.IntegerField(default=0)
	reco_code=models.CharField(max_length=6,null=True)
	

class selldb(models.Model):
	Cus_id=models.CharField(max_length=32) 
	Q_price=models.CharField(max_length=50)
	Sel_type=models.IntegerField(default=False)
	Sel_comments=models.CharField(max_length=500)
 	Sel_deltype=models.IntegerField(default=False)
	Cus2_conf=models.IntegerField(default=False)
	Sel2_conf=models.IntegerField(default=False)
	img_path=models.CharField(max_length=100,null=True)
	Sel_id=models.CharField(max_length=32,primary_key=True)
	Sel_email=models.EmailField(null=True)
	bprice=models.CharField(max_length=200,default=0)
	btime=models.BigIntegerField(default=0)
	delivery=models.IntegerField(default=0)
	pickup=models.IntegerField(default=0)
	bargaintimes=models.IntegerField(default=0)

class adv(models.Model):
	advt=models.CharField(max_length=2000,null=True)
	edate=models.CharField(max_length=100)
	sdate=models.CharField(max_length=100)
	email=models.CharField(max_length=100,null=True)

class feed(models.Model):
	feedb=models.CharField(max_length=2000,null=True)
	email=models.CharField(max_length=100,null=True)



class accesscode(models.Model):
	access=models.CharField(max_length=100,null=True)

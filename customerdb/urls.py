"""customerdb URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from cd.views import *

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^front/',front),
    url(r'^login/',sellerlogin),
    url(r'^signup/',sellersignup),
    url(r'^gcmid/',sellergcm),
    url(r'^category/',sellercategory),
    url(r'^c2s/',customer2seller),
    url(r'^s2c/',s2c),
    url(r'^cus_conf/',cus_conf),
    url(r'^clink/',item),
    url(r'^decline/',decline),
    url(r'^userdata/',userdata),
    url(r'^updateuser/',updateuser),
    url(r'^adv/',advt),
    url(r'^feed/',feed),
    url(r'^bargain/',bargain),
    url(r'^selconf/',sel_conf),
    url(r'^cusgcm/',customergcm),
    url(r'^cuslogin/',customerlogin),
    url(r'^cussignup/',customersignup),
    url(r'^del/',delivery),
    url(r'^pickup/',pickup),
    url(r'^returns/',sellerreturns),
    url(r'^recommend_sel/',recommend_sel),
    url(r'^recommend/',recommend),
    url(r'^forgotpassword_sel/',forgotpass_sel),
    url(r'^passverification_sel/',passverification),
    url(r'^passchange_sel/',passchange_sel),
    url(r'^returnpolicy/',returnpolicy),
    url(r'^accesscode/',accesscode),
    
    


    

 #   url(r'^simg/',simg),    


]
